﻿using System;
using System.Linq;
using System.Net.Http;
using System.Windows.Forms;

namespace KladrTesting
{
    public partial class Form1 : Form
    {
        private DateTime ReqTime { get; set; }
        private DateTime RespTime { get; set; }

        public Form1()
        {
            InitializeComponent();
        }

        private async void GetKlagr(SearchContext search)
        {
            var client = new HttpClient();
            try
            {
                var response = await client.GetAsync("http://kladr-api.ru/api.php?" + search);
                response.EnsureSuccessStatusCode();
                var responseBody = await response.Content.ReadAsStringAsync();

                RespTime = DateTime.Now;
                var reqText = GetTextFromUtf8(responseBody);
                textBox2.Text = @"Raw:" + Environment.NewLine + reqText;

                // test deserialize
                var serializer = new JsonSerializer();
                var result = serializer.Deserialize<SearchResult>(reqText);
                var cities = result.Result;
                string foundedCities = Environment.NewLine + "Cities: {";
                foundedCities = cities.Aggregate(foundedCities, (current, city) => current + (city.Name + ", "));
                foundedCities += "}";
                textBox2.Text += foundedCities;

                textBox2.Text += Environment.NewLine + $@"Elapsed time = {(RespTime - ReqTime).TotalMilliseconds} ms";
            }
            catch (HttpRequestException e)
            {
                textBox2.Text += string.Format("\nException Caught!");
                textBox2.Text += $@"Message :{e.Message} ";
            }
        }

        private string GetTextFromUtf8(string orig)
        {
            return System.Text.RegularExpressions.Regex.Unescape(orig);
        }

        private void comboBox1_TextUpdate(object sender, EventArgs e)
        {
            DoSearch();
        }

        private void DoSearch()
        {
            var context = new SearchContext
            {
                Content = SearchContext.ContentType.City,
                Limit = 5,
                WithParent = checkBox_withParent.Checked,
                Query = comboBox1.Text
            };

            GetKlagr(context);

            ReqTime = DateTime.Now;
        }

        private void checkBox_withParent_CheckedChanged(object sender, EventArgs e)
        {
            DoSearch();
        }
    }
}