﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace KladrTesting
{
    public class JsonSerializer
    {
        private readonly Newtonsoft.Json.JsonSerializer jsonSerializer;

        public JsonSerializer()
        {
            jsonSerializer = new Newtonsoft.Json.JsonSerializer();
        }

        public void Serialize(System.IO.Stream stream, object obj, Type objType)
        {
            try
            {
                var streamWriter = new StreamWriter(stream);
                var jsonTextWriter = new JsonTextWriter(streamWriter);

                jsonSerializer.Serialize(jsonTextWriter, obj, objType);
                jsonTextWriter.Flush();
            }
            catch (Exception e)
            {
                var message = string.Format("Fail to serialize object of type {0}", obj.GetType().Name);
                throw new SerializationException(message, e);
            }
        }

        public byte[] Serialize(object obj, Type objType)
        {
            using (var memoryStream = new MemoryStream())
            {
                Serialize(memoryStream, obj, objType);
                return memoryStream.ToArray();
            }
        }

        public string Stringify(object obj, Type objType)
        {
            var bytes = Serialize(obj, objType);
            return Encoding.UTF8.GetString(bytes);
        }

        public object Deserialize(System.IO.Stream stream, Type objectType)
        {
            var streamReader = new StreamReader(stream);
            if (streamReader.EndOfStream)
            {
                throw new SerializationException("Fail to deserialize object from empty bytes array.");
            }

            try
            {
                var jsonTextReader = new JsonTextReader(streamReader);

                return jsonSerializer.Deserialize(jsonTextReader, objectType);
            }
            catch (Exception e)
            {
                throw new SerializationException(string.Format("Fail to deserialize type {0}", objectType.Name), e);
            }
        }

        public object Deserialize(byte[] bytes, Type objectType)
        {
            using (var memoryStream = new MemoryStream(bytes))
            {
                return Deserialize(memoryStream, objectType);
            }
        }

        public T Deserialize<T>(string serializedObj)
        {
            var bytes = Encoding.UTF8.GetBytes(serializedObj);
            return (T)Deserialize(bytes, typeof(T));
        }

        public T Deserialize<T>(System.IO.Stream stream)
        {
            return (T)Deserialize(stream, typeof(T));
        }

        public T Deserialize<T>(byte[] bytes)
        {
            return (T)Deserialize(bytes, typeof(T));
        }
    }
}
