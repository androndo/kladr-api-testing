﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KladrTesting
{
    public class City
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public int Zip { get; set; }
        public long Okato { get; set; }
    }
}
