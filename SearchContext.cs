﻿using System;

namespace KladrTesting
{
    public class SearchContext
    {
        public string Query { get; set; }

        public enum ContentType
        {
            Region,
            District,
            City,
            Street,
            Building
        }

        public ContentType Content { get; set; }
        public bool WithParent { get; set; }

        // лимит результатов поиска
        public int Limit { get; set; }

        // todo: need to be tested
        public override string ToString()
        {
            string contentKeyword;
            switch (Content)
            {
                case ContentType.City:
                    contentKeyword = "city";
                    break;

                case ContentType.Region:
                    contentKeyword = "region";
                    break;

                case ContentType.District:
                    contentKeyword = "district";
                    break;

                case ContentType.Street:
                    contentKeyword = "street";
                    break;

                case ContentType.Building:
                    contentKeyword = "building";
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
            return $"query={Query}&contentType={contentKeyword}&withParent={Convert.ToInt32(WithParent ? 1 : 0)}&limit={Limit}";
        }
    }
}
